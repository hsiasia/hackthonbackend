package com.hackthon.backend.model;

import lombok.*;
import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @NonNull
    private String userName;

    @NonNull
    private Long officeId;

    private Long prize;

//    public void updateprize(Long score){
//        Long newPrize = this.getPrize()+score;
//        this.setPrize(newPrize);
//    }
}