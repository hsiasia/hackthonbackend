package com.hackthon.backend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TwoData {


    private String First;

    private String Second;

    @JsonProperty(value = "First")
    public String getFirst() {
        return First;
    }

    @JsonProperty(value = "First")
    public void setFirst(String First) {
        this.First = First;
    }

    @JsonProperty(value = "Second")
    public String getSecond() {
        return Second;
    }

    @JsonProperty(value = "Second")
    public void setSecond(String Second) {
        this.Second = Second;
    }
}