package com.hackthon.backend.model;

import lombok.*;
import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "note")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long noteId;

    @NonNull
    private String content;

    @NonNull
    private Long officeId;

    @NonNull
    private String author;
}