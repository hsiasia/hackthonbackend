package com.hackthon.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OneData {

    private String First;

    @JsonProperty(value = "First")
    public String getFirst() {
        return First;
    }

    @JsonProperty(value = "First")
    public void setFirst(String First) {
        this.First = First;
    }
}