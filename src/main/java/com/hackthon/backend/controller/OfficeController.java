package com.hackthon.backend.controller;

import com.hackthon.backend.dao.OfficeRepository;
import com.hackthon.backend.model.Note;
import com.hackthon.backend.model.Office;
import com.hackthon.backend.model.OneData;
import com.hackthon.backend.model.TwoData;
import com.hackthon.backend.service.OfficeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class OfficeController {

    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private OfficeService officeService;

    @GetMapping("/offices")
    public ResponseEntity<List<Office>> Offices() {
        List<Office> office = officeRepository.findAll();
        return ResponseEntity.ok().body(office);
    }

    @GetMapping("/office/{id}")
    public ResponseEntity<Office> getOffice(@PathVariable Long id) {
        Office office = officeRepository.findById(id).get();
        return ResponseEntity.ok().body(office);
    }

//    TESTing
    @PostMapping("/office")
    public ResponseEntity<Office> createOffice(@RequestBody TwoData twoData) throws Exception {
        Office result = officeService.CreatOffice(twoData.getFirst(),twoData.getSecond());
        return ResponseEntity.ok().body(result);
    }

//    need return
    @DeleteMapping("/office")
    public ResponseEntity<?> deleteOffice(@RequestBody OneData id){
        Long Newid = Long.parseLong(id.getFirst());
        officeRepository.deleteById(Newid);
        return ResponseEntity.ok().build();
    }
}