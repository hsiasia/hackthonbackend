package com.hackthon.backend.controller;

import com.hackthon.backend.dao.UserRepository;
import com.hackthon.backend.model.OneData;
import com.hackthon.backend.model.User;
import com.hackthon.backend.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public ResponseEntity<List<User>> Users(@PathVariable Long id) {
        List<User> user = userRepository.getByofficeId(id);
        return ResponseEntity.ok().body(user);
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestBody OneData score) throws Exception {
        User newuser = userRepository.findById(id).get();
        User result = userService.UpdatePrize(newuser, score.getFirst());
        return ResponseEntity.ok().body(result);
    }
}