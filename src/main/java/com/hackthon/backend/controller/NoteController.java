package com.hackthon.backend.controller;

import com.hackthon.backend.dao.NoteRepository;
import com.hackthon.backend.model.Note;
import com.hackthon.backend.model.OneData;
import com.hackthon.backend.model.TwoData;
import com.hackthon.backend.service.NoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class NoteController {

    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private NoteService noteService;

    @GetMapping("/notes/{id}")
    public ResponseEntity<List<Note>> Notes(@PathVariable Long id) {
        List<Note> note = noteService.SearchNotes(id);
        return ResponseEntity.ok().body(note);
    }

    @GetMapping("/note/{id}")
    public ResponseEntity<?> getNote(@PathVariable Long id) {
        Optional<Note> note = noteRepository.findById(id);
        return note.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/note")
    public ResponseEntity<Note> createNote(@RequestBody Note note) throws Exception {
//        System.out.println("object:"+note);
        Note result = noteRepository.save(note);
        return ResponseEntity.ok().body(result);
    }

//    TESTing
    @PutMapping("/note")
    public ResponseEntity<Note> updateNote(@RequestBody TwoData twoData) throws Exception {
        Long Newid = Long.parseLong(twoData.getFirst());
        Note result = noteRepository.findById(Newid).get();
        result.setContent(twoData.getSecond());
        noteRepository.save(result);
        return ResponseEntity.ok().body(result);
    }

//    need return
    @DeleteMapping("/note")
    public ResponseEntity<?> deleteNote(@RequestBody OneData id) {
        Long Newid = Long.parseLong(id.getFirst());
        noteRepository.deleteById(Newid);
        return ResponseEntity.ok().build();
    }
}