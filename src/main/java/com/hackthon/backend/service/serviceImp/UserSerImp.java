package com.hackthon.backend.service.serviceImp;

import com.hackthon.backend.dao.UserRepository;
import com.hackthon.backend.model.User;
import com.hackthon.backend.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSerImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User UpdatePrize(User user ,String score) {
        Long Newscore = Long.parseLong(score);
        Long newPrize = user.getPrize()+Newscore;
        user.setPrize(newPrize);
        userRepository.save(user);
        return user;
    }
}