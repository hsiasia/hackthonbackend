package com.hackthon.backend.service.serviceImp;

import com.hackthon.backend.dao.OfficeRepository;
import com.hackthon.backend.model.Office;
import com.hackthon.backend.service.OfficeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfficeSerImp implements OfficeService {

    @Autowired
    private OfficeRepository officeRepository;

    @Override
    public Office CreatOffice(String id,String name){
        Long Newid = Long.parseLong(id);
        Office office = new Office();
        office.setOfficeName(name);
        office.setUserId(Newid);
        officeRepository.save(office);
        return office;
    }

//    @Override
//    public Office saveOffice(Office office) {
//        office.setOfficeId();
//        office.setOfficeName(name);
//        office.setUser(user);
//        officeRepository.save(office);
//        return office;
//    }
}