package com.hackthon.backend.service.serviceImp;

import com.hackthon.backend.dao.NoteRepository;
import com.hackthon.backend.model.Note;
import com.hackthon.backend.model.User;
import com.hackthon.backend.service.NoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteSerImp implements NoteService {

    @Autowired
    private NoteRepository noteRepository;

    @Override
    public List<Note> SearchNotes(Long id) {
//        Long Newid = Long.parseLong(id);
        List<Note> note = noteRepository.getByofficeId(id);
        return note;
    }
}