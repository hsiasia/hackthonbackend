package com.hackthon.backend.service;

import com.hackthon.backend.model.Note;

import java.util.List;

public interface NoteService {
    public List<Note> SearchNotes(Long id);
}
